SPECFILE             = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
DIST                ?= $(shell rpm --eval %{dist})
_KOJI_OS            ?= $(shell rpm --eval %{rhel})

clean:
	rm -rf build/ *.tgz

sources:
	tar -zcvf $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz --exclude-vcs --transform 's,^src/,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' src/*

rpm: sources
	sed -i "s/KOJI_OS/${_KOJI_OS}/g" $(SPECFILE)
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' $(SPECFILE)

srpm: sources
	sed -i "s/KOJI_OS/${_KOJI_OS}/g" $(SPECFILE)
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' $(SPECFILE)
