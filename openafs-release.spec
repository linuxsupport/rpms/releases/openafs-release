%define product openafs
%define debug_package %{nil}
%define distOS KOJI_OS

Name:		%{product}-release
Version:	1.4
Release:	1%{?dist}
Summary:	CERN %{product} repository release file
Group:		System Environment/Base
License:	GPLv2
URL:		http://linux.cern.ch/
BuildArch:  noarch
Source0:	%{product}.repo

%description
The package contains yum configuration for the CERN %{product} repository

%prep
%setup -q  -c -T
install -pm 0644 %{SOURCE0} .
# Why no elif, you ask? Because rpm on C8 apparently doesn't support it.
%if "%{distOS}" == "8s"
    %define DNFVARIABLE cernstream8
    %define DNFTESTING s8
    %define DNFDISTRO centos
%endif
%if "%{distOS}" == "8el"
    %define DNFVARIABLE cernrhel
    %define DNFTESTING 8
    %define DNFDISTRO rhel
%endif
%if "%{distOS}" == "8al"
    %define DNFVARIABLE cernalmalinux
    %define DNFTESTING 8
    %define DNFDISTRO alma
%endif
%if "%{distOS}" == "9"
    %define DNFVARIABLE cernstream9
    %define DNFTESTING s9
    %define DNFDISTRO centos
%endif
%if "%{distOS}" == "9el"
    %define DNFVARIABLE cernrhel
    %define DNFTESTING 9
    %define DNFDISTRO rhel
%endif
%if "%{distOS}" == "9al"
    %define DNFVARIABLE cernalmalinux
    %define DNFTESTING 9
    %define DNFDISTRO alma
%endif
sed -i 's/DNFVARIABLE/%{DNFVARIABLE}/g' %{product}.repo
sed -i 's/DNFTESTING/%{DNFTESTING}/g' %{product}.repo
sed -i 's/DNFDISTRO/%{DNFDISTRO}/g' %{product}.repo

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 0755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 0644 %{product}.repo $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d/%{product}.repo

%files
%defattr(-,root,root,-)
%doc
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Wed Nov 30 2022 Alex Iribarren <Alex.Iribarren@cern.ch> 1.4-1
- added support for RHEL8 and AlmaLinux 8/9

* Tue Sep 06 2022 Alex Iribarren <Alex.Iribarren@cern.ch> 1.3-1
- added support for RHEL9

* Wed Nov 03 2021 Alex Iribarren <Alex.Iribarren@cern.ch> 1.2-1
- added support for CS9
- fixed testing repo on CS8

* Tue Feb 18 2020 Ben Morrice <ben.morrice@cern.ch> 1.1-1
- move repo to use $cernbase variable

* Fri Nov 22 2019 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
